function get_user() {
    local user="${USER}"
    if [ ${user} = "root" ]; then
        user="${SUDO_USER}"
    fi

    echo ${user}
}

function get_home_directory() {
    local user="$(get_user)"
    eval echo ~${user}
}

function get_config_dir() {
    local dir="${DEMODOCK_CONFIG}"
    if [ -z "${dir}" ]; then
        dir=$(get_home_directory)/.config/demodock
    fi

    echo ${dir}
}

function get_docker_binary() {
    local docker="${DEMODOCK_DOCKER_BINARY}"
    if [ -z "${DEMODOCK_DOCKER_BINARY}" ]; then
        docker=docker
    fi

    echo ${docker}
}

function get_nginx_binary() {
    local nginx="${DEMODOCK_NGINX_BINARY}"
    if [ -z "${DEMODOCK_NGINX_BINARY}" ]; then
        nginx=nginx
    fi

    echo ${nginx}
}

function get_python_binary() {
    local python="${DEMODOCK_PYTHON_BINARY}"
    if [ -z "${DEMODOCK_PYTHON_BINARY}" ]; then
        python=python
    fi

    echo ${python}
}

function get_timestamp() {
    date +"%Y%m%d_%H%M%S_%N"
}

function get_pid() {
    pid=$(cat $(get_config_dir)/nginx/logs/nginx.pid 2>/dev/null) || true
    echo ${pid}
}

function get_config_file() {
    conf=$(cat ${config}/conf-file 2>/dev/null) || true
    echo ${conf}
}

function get_live_demos() {
    docker="$(get_docker_binary)"

    ${docker} ps | sed -e '1p' -e '1d' -e '/demodock\//!d'
}
