#!/bin/bash
set -e
here=$(cd $(dirname $0); pwd)
source "${here}/../lib/ddlib.sh"

root="$1"
port="$2"

user=$(get_user)
config=$(get_config_dir)

temp=`mktemp ${config}/nginx-XXXXXXXX.conf`

cat <<EOT >${temp}
worker_processes 1;
pid logs/nginx.pid;
error_log logs/error.log;
lock_file ${config}/nginx/nginx.lock;

events {
    worker_connections 768;
}

http {
    #include /etc/nginx/mime.types;
    default_type application/octet-stream;
    sendfile on;
    keepalive_timeout 65;
    access_log logs/access.log;
    log_not_found off;

    server {
        listen ${port} default_server;
        listen [::]:${port} default_server ipv6only=on;
        client_max_body_size 2G;
        root "${root}";
        index index.html index.htm;
        proxy_connect_timeout 3600;
        proxy_send_timeout    3600;
        proxy_read_timeout    3600;
        send_timeout          3600;
        proxy_buffering off;
        # Make site accessible from http://localhost/
        server_name localhost;
        server_tokens off;
        include nginx_proxy_list;
        location / {
            # First attempt to serve request as file, then
            # as directory, then fall back to displaying a 404.
            try_files \$uri \$uri/ =404;
        }
    }
}
EOT

cat ${temp} >&2

echo ${temp}
